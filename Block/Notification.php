<?php
namespace RedChamps\FaviconNotification\Block;

class Notification extends \Magento\Framework\View\Element\Template
{
    const XML_BASE_PATH = 'favicon_notification/settings/';
    protected $cartHelper;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Helper\Cart $cartHelper
    )
    {
        $this->cartHelper = $cartHelper;
        parent::__construct($context);
    }

    public function getCartCount()
    {
        return $this->cartHelper->getItemsQty();
    }

    public function isEnabled()
    {
        return  $this->_scopeConfig->getValue(self::XML_BASE_PATH."enabled", \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}