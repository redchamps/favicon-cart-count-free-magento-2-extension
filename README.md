# Favicon Cart Count Free Magento 2 Extension

Decrease the number of your store's abandoned carts by adding a notification that will appear directly in the customer's browser.

This Magento 2 extension can be installed and configured in just 30 seconds and displays the number of products in a customer's cart next to your store's favicon. It's a discrete and effective nudge to remind users they have a full cart and encourage them to check out!

##Installation Instructions

This extension can be manually installed by uploading file system via FTP or can be installed by executing below commands via SSH in your Magento 2 root directory.

1. composer require redchamps/faviconnotification
2. php bin/magento setup:upgrade

Please note that this extension doesn't support ajax add to cart and we have pro version of this extension for that feature. The pro version can be found at below link 

[Favicon Cart Count Pro Magento 2 Extension](https://redchamps.com/favicon-cart-count-pro-magento-2-extension.html)

Also, this extension is available for Magento 1 store as URL. Product page URL's are:

Free Version: https://redchamps.com/favicon-cart-count-free-magento-1-extension.html 

Pro Version: https://redchamps.com/favicon-cart-count-pro-magento-1-extension.html

Visit our store for more extensions [RedChamps.com](https://redchamps.com)