<?php
namespace RedChamps\FaviconNotification\Model\Config\Source;

class FontStyle implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'normal', 'label' => __('Normal')],
            ['value' => 'italic', 'label' => __('Italic')],
            ['value' => 'bold', 'label' => __('Bold')],
            ['value' => 'bolder', 'label' => __('Bolder')],
            ['value' => 'oblique', 'label' => __('Oblique')],
            ['value' => 'lighter', 'label' => __('Lighter')],
            ['value' => '100', 'label' => __('100')],
            ['value' => '200', 'label' => __('200')],
            ['value' => '300', 'label' => __('300')],
            ['value' => '400', 'label' => __('400')],
            ['value' => '500', 'label' => __('500')],
            ['value' => '600', 'label' => __('600')],
            ['value' => '700', 'label' => __('700')],
            ['value' => '800', 'label' => __('800')],
            ['value' => '900', 'label' => __('900')]
        ];
    }
}