<?php
namespace RedChamps\FaviconNotification\Model\Config\Source;

class Animation implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'none', 'label' => __('None')],
            ['value' => 'slide', 'label' => __('Slide')],
            ['value' => 'fade', 'label' => __('Fade')],
            ['value' => 'pop', 'label' => __('Pop')],
            ['value' => 'popFade', 'label' => __('Pop & Fade')]
        ];
    }
}