<?php
namespace RedChamps\FaviconNotification\Model\Config\Source;

class FontFamily implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'Arial', 'label' => __('Arial')],
            ['value' => 'Verdana', 'label' => __('Verdana')],
            ['value' => 'Times New Roman', 'label' => __('Times New Roman')],
            ['value' => 'serif', 'label' => __('Serif')],
            ['value' => 'sans-serif', 'label' => __('Sans-Serif')]
        ];
    }
}