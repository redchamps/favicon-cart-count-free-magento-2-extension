<?php
namespace RedChamps\FaviconNotification\Model\Config\Source;

class Shape implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 'circle', 'label' => __('Circle')], ['value' => 'rectangle', 'label' => __('Rectangle')]];
    }
}