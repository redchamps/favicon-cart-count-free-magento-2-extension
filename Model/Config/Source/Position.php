<?php
namespace RedChamps\FaviconNotification\Model\Config\Source;

class Position implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'up', 'label' => __('Top')],
            ['value' => 'down', 'label' => __('Bottom')],
            ['value' => 'left', 'label' => __('Left')],
            ['value' => 'upleft', 'label' => __('Top Left')]
        ];
    }
}